# creating an EC2 instance in the mumbai region using terraform

# configure the aws provider

provider "aws" {
    region = "ap-south-1"
    access_key = "AKIAX7WZEZHS37QKGSVV"
    secret_key = "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"
}
#creat an EC2 server
resource "aws_instance" "terraform_instance" {
  ami           = "ami-0a23ccb2cdd9286bb"
  instance_type = "t2.micro"
  key_name = "Mumbaikey"
  tags = {
    Name = "terraform_instance"
  }
}
#creating a s3 bucket
resource "aws_s3_bucket" "ameekh-bucket" {
    bucket = "ameekh-bucket"

  tags = {
    Name        = "ameekh-bucket"
  }
}
# uploading an opject to my s3 bucket
resource "aws_s3_bucket_object" "object" {
  bucket = "ameekh-bucket"
  key    = "new_image"
  source = "D:/tree-736885__480.jpg"
}
resource "aws_vpc" "main" {
  cidr_block       = "192.168.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "terraform_VPC"
  }
}
resource "aws_subnet" "main" {
  vpc_id     = "vpc-0d890ccbf3ce31b07"
  cidr_block = "192.168.1.0/24"
  availability_zone = "ap-south-1a"

  tags = {
    Name = "Public_subnet"
  }
}
resource "aws_subnet" "secondary" {
  vpc_id     = "vpc-0d890ccbf3ce31b07"
  cidr_block = "192.168.2.0/24"
  availability_zone = "ap-south-1a"

  tags = {
    Name = "Private_subnet"
  }
}
resource "aws_internet_gateway" "gw" {
  vpc_id = "vpc-0d890ccbf3ce31b07"

  tags = {
    Name = "Terraform_Internet_Gateway"
  }
}
resource "aws_route_table" "public" {
  vpc_id = "vpc-0d890ccbf3ce31b07"

  tags = {
    Name = "Public_Route_Table"
  }
}
resource "aws_route_table" "private" {
  vpc_id = "vpc-0d890ccbf3ce31b07"

  tags = {
    Name = "Private_Route_Table"
  }
}
resource "aws_eip" "lb" {
  instance = "i-0a713bc1cb1710689"
  vpc      = "true"
}

resource "aws_nat_gateway" "example" {
  connectivity_type = "public"
  allocation_id = "eipalloc-079f47afbd33f91e7"
  subnet_id     = "subnet-02aacff5f4320ba85"

  tags = {
    Name = "NAT_Gateway"
  }
}
resource "aws_route_table_association" "public_route_table_associatioa_a" {
  subnet_id      = "subnet-02aacff5f4320ba85"
  route_table_id = "rtb-06ff65d80b24f8cad"
}
resource "aws_route_table_association" "private_route_table_association_b" {
  subnet_id      = "subnet-0754db7d1f2793a5b"
  route_table_id = "rtb-07867044c8bd94df8"
}
resource "aws_route_table_association" "public_route_table_associatioa_c" {
  gateway_id      = "igw-03039de27af38efd3"
  route_table_id = "rtb-06ff65d80b24f8cad"
}


